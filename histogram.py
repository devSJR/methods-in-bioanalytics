#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 28 11:19:13 2019

@author: Stefan Rödiger
"""

# Working with images
# For the installation please follow the instruction given by the 
# package authors.

from PIL import Image
import matplotlib.pyplot as plt

# Define the location of the image
img  = Image.open("Well_Slide1_3_FITC.png")

img

# Get the histogram data
img.histogram()

# assign the histogram data to the object his and plot the data as histogram
his = img.histogram()
plt.hist(his)
plt.xlabel('Intensity')
plt.ylabel('Frequency')

