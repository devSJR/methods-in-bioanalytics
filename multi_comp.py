#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 26 08:23:29 2019

@author: Stefan Rödiger
"""

# The numpy package is used for dealing with data
import numpy as np
# The statsmodels package contains the functions to calculate the statistics
# of the TukeyHSD-test
from statsmodels.stats.multicomp import (pairwise_tukeyhsd, MultiComparison)
# To get the p-values
from statsmodels.stats.libqsturng import psturng

# Create a simple expample
data = np.array([('ETP', 2.6), ('ETP', 4), ('ETP', 6), ('ETP', 1),
                 ('Cis', 10), ('Cis', 12), ('Cis', 15),
                 ('cyc', 1), ('cyc', 0.1), ('cyc', 1), ('cyc', 2)], 
                dtype=[('treatment', '|S8'), ('foci', '<i4')])

res = pairwise_tukeyhsd(data['foci'], data['treatment'])
res.plot_simultaneous()

print("MultiComparison")
print("----------------------------------------------------------")
res = MultiComparison(data['foci'], data['treatment'])
tukey_res = res.tukeyhsd()
summary = tukey_res.summary()
print("summary:", summary)
print("mean diffs:", tukey_res.meandiffs)
print("std pairs:",tukey_res.std_pairs)
print("groups unique: ", tukey_res.groupsunique)
print("df total:", tukey_res.df_total)
# Formular for the p-value
p_values = psturng(np.abs(tukey_res.meandiffs / tukey_res.std_pairs), len(tukey_res.groupsunique), tukey_res.df_total)
print("p values:", p_values)


# the scikit-posthocs package (Terpilowski 2019, JOSS, 
# https://doi.org/10.21105/joss.01169) provides an alternative approach

import scikit_posthocs as sp

foci = [[2.6,4,6,1], [10,12,15], [1, 0.1, 1,2]]
treatment = [['ETP'] * 4, ['Cis'] * 3, ['cyc'] * 4]

sp.posthoc_tukey_hsd(np.concatenate(foci), np.concatenate(treatment), 
                     alpha = 0.05)
